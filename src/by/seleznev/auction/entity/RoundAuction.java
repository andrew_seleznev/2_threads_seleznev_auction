package by.seleznev.auction.entity;

import java.util.ArrayList;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;

public class RoundAuction extends Thread {
	
	static Logger logger = Logger.getLogger(RoundAuction.class);
	public final int INIT_LOT_PRICE;
	public final int AMOUNT_PARTICIPANT;
	private AtomicInteger lotPrice;
	private AtomicInteger stepPercent;
	private AtomicInteger lastParticipant;
	private AtomicInteger count;
	private AtomicBoolean isBiddingTime;
	private String lotName;
	private int idRound;
	private int biddingTime;
	private Lock lock = new ReentrantLock();
	private Condition condition = lock.newCondition();
	private ArrayList<Participant> participants;
	private CyclicBarrier barrier;

	public RoundAuction(int idRound, int biddingTime, int lotPrice, int stepPercent, String lotName,
			ArrayList<Participant> participants, CyclicBarrier barrier) {
		this.idRound = idRound;
		this.lotPrice = new AtomicInteger(lotPrice);
		this.stepPercent = new AtomicInteger(stepPercent);
		this.lotName = lotName;
		this.biddingTime = biddingTime;
		this.participants = participants;
		this.barrier = barrier;
		lastParticipant = new AtomicInteger();
		count = new AtomicInteger();
		isBiddingTime = new AtomicBoolean();
		INIT_LOT_PRICE = lotPrice;
		AMOUNT_PARTICIPANT = participants.size();
	}

	@Override
	public void run() {
		lock.lock();
		try {
			isBiddingTime.set(true);
			startAllParticipantsBidding();
			if (condition.await(getBiddingTime(), TimeUnit.SECONDS)) {
				logger.info("All bets have been made");
				isBiddingTime.set(false);
			} else {
				logger.info("Bidding time is over");
				isBiddingTime.set(false);
			}
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
		} finally {
			lock.unlock();
		}
	}

	private void startAllParticipantsBidding() {
		for (Participant participant : participants) {
			participant.startBidding();
		}
	}

	public void finalBidMade() {
		lock.lock();
		count.incrementAndGet();
		try {
			if (barrier.getNumberWaiting() == AMOUNT_PARTICIPANT) {
				condition.signal();
			}
		} finally {
			lock.unlock();
		}
	}

	public boolean raiseBid(int newPrice, int newPercent, int id) {
		lock.lock();
		try {
			if (newPrice > lotPrice.get()) {
				System.out.println("+ Participant " + id + " raise bid, current price: " + lotPrice.get()
						+ ", new price:" + newPrice + ", percent: " + newPercent);
				lotPrice.set(newPrice);
				stepPercent.set(newPercent);
				lastParticipant.set(id);
				return true;
			} else {
				return false;
			}
		} finally {
			lock.unlock();
		}
	}

	public int getStepPercent() {
		return stepPercent.get();
	}

	public int getIdRound() {
		return idRound;
	}

	public int getLotPrice() {
		return lotPrice.get();
	}

	public int getLastParticipant() {
		return lastParticipant.get();
	}

	public boolean isBiddingTime() {
		return isBiddingTime.get();
	}

	public String getLotName() {
		return lotName;
	}

	public int getCount() {
		return count.get();
	}

	public int getBiddingTime() {
		return biddingTime;
	}
}
