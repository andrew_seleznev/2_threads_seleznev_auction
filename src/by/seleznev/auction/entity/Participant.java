package by.seleznev.auction.entity;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Random;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import org.apache.log4j.Logger;

public class Participant extends Thread {

	static Logger logger = Logger.getLogger(Participant.class);
	public final int INIT_MONEY;
	private int idParticipant;
	private int totalMoney;
	private int spendMoney;
	private int roundsNumber;
	private RoundAuction round;
	private CyclicBarrier barrier;
	private Lock lock = new ReentrantLock();
	private Condition condition = lock.newCondition();
	private HashSet<Integer> interestSet;
	private HashMap<String, Integer> purchases;

	public Participant(int idParticipant, int totalMoney, CyclicBarrier barrier, int roundsNumber,
			HashSet<Integer> interestSet) {
		this.idParticipant = idParticipant;
		this.totalMoney = totalMoney;
		this.barrier = barrier;
		this.roundsNumber = roundsNumber;
		this.interestSet = interestSet;
		INIT_MONEY = totalMoney;
		purchases = new HashMap<>();
	}

	@Override
	public void run() {
		lock.lock();
		try {
			int round = 0;
			while (round < roundsNumber) {
				logger.info(
						"Participant: " + idParticipant + " - has been started and interested in lots: " + interestSet);
				doBidding();
				round += 1;
			}
		} finally {
			lock.unlock();
		}

	}

	public void startBidding() {
		lock.lock();
		try {
			condition.signal();
		} finally {
			lock.unlock();
		}
	}

	public void stopBidding() {
		try {
			logger.info("Participant " + idParticipant + " stop bidding and awaiting the results of the round");
			barrier.await();
		} catch (InterruptedException e) {
			logger.error(e.getMessage() + "has been interrupted");
		} catch (BrokenBarrierException e) {
			logger.error(e.getMessage());
		}
	}

	public void doBidding() {
		lock.lock();
		try {
			condition.await();
			while (round.isBiddingTime()) {
				if (isInterestingLot()) {
					if (idParticipant != round.getLastParticipant()) {
						TimeUnit.SECONDS.sleep(2);
						int stepPercent = round.getStepPercent();
						int lotPrice = round.getLotPrice();
						int simpleBid = lotPrice + round.INIT_LOT_PRICE * stepPercent / 100;
						int doubleBid = lotPrice + round.INIT_LOT_PRICE * 2 * stepPercent / 100;
						boolean isDoubleRaise = new Random().nextBoolean();
						if (totalMoney > simpleBid) {
							if (isDoubleRaise) {
								if (totalMoney > doubleBid) {
									if (round.raiseBid(doubleBid, stepPercent * 2, idParticipant)) {
										spendMoney = doubleBid;
									}
								} else if (round.raiseBid(simpleBid, stepPercent, idParticipant)) {
									spendMoney = simpleBid;
								}
							} else if (round.raiseBid(simpleBid, stepPercent, idParticipant)) {
								spendMoney = simpleBid;
							}
						} else {
							round.finalBidMade();
							break;
						}
					}
				} else {
					logger.info("Participant " + idParticipant + " isn't interested in lot");
					break;
				}
			}
			logger.info("Participant " + idParticipant + " stop bidding and awaiting the results of the round");
			barrier.await();
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
		} catch (BrokenBarrierException e) {
			logger.error(e.getMessage());
		} finally {
			lock.unlock();
		}
	}

	private boolean isInterestingLot() {
		return (interestSet.contains(round.getIdRound())) ? true : false;
	}

	public void countMoney(int id) {
		if (id == idParticipant) {
			totalMoney -= spendMoney;
		}
		spendMoney = 0;
	}

	public HashMap<String, Integer> getPurchases() {
		return purchases;
	}

	public void addPurchases() {
		purchases.put(round.getLotName(), spendMoney);
	}

	public int getIdParticipant() {
		return idParticipant;
	}

	public Condition getCondition() {
		return condition;
	}

	public int getTotalMoney() {
		return totalMoney;
	}

	public RoundAuction getRoundAuction() {
		return round;
	}

	public void setRoundAuction(RoundAuction roundAuction) {
		this.round = roundAuction;
	}

	public HashSet<Integer> getInterestSet() {
		return interestSet;
	}

	public int getSpendMoney() {
		return spendMoney;
	}
}
