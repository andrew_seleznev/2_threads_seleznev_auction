package by.seleznev.auction.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.log4j.Logger;

import by.seleznev.auction.report.ReportAuction;

public class Auction extends Thread {

	static Logger logger = Logger.getLogger(Auction.class);
	public final int PARTICIPANTS_NUMBER = 20;
	private ArrayList<Participant> participants;
	private ArrayList<RoundAuction> rounds;
	private CyclicBarrier barrier;
	private AtomicBoolean isFinished;

	private Auction() {
		participants = new ArrayList<>();
		rounds = new ArrayList<>();
		barrier = new CyclicBarrier(PARTICIPANTS_NUMBER, new AuctionBarrierAction());
		isFinished = new AtomicBoolean(false);
	}

	private static class SingletonHolder {
		private final static Auction INSTANCE = new Auction();
	}

	public static Auction getInstance() {
		return SingletonHolder.INSTANCE;
	}

	@Override
	public void run() {
		for (int i = 0; i < rounds.size(); i++) {
			RoundAuction roundAuction = rounds.get(i);
			startNewRound(roundAuction);
			logger.info("Round " + roundAuction.getIdRound() + " start");
			roundAuction.start();
			while (true) {
				if (isFinished()) {
					isFinished.set(false);
					break;
				}
			}
		}
		ReportAuction.printFinalReport(participants);
	}

	private void startNewRound(RoundAuction newRound) {
		for (Iterator<Participant> iterator = participants.iterator(); iterator.hasNext();) {
			iterator.next().setRoundAuction(newRound);
		}
	}

	public Participant defineWinner() {
		return Collections.max(participants, new Comparator<Participant>() {
			@Override
			public int compare(Participant ob1, Participant ob2) {
				return ob1.getSpendMoney() - ob2.getSpendMoney();
			}
		});
	}

	private class AuctionBarrierAction implements Runnable {

		@Override
		public void run() {
			if (isLotSold()) {
				Participant winner = defineWinner();
				winner.addPurchases();
				ReportAuction.printParticipantStatistic(participants, winner);
			} else {
				ReportAuction.printUnsoldLotInformation(participants);
			}
			isFinished.set(true);
		}
	}

	private boolean isLotSold() {
		Iterator<Participant> iterator = participants.iterator();
		while (iterator.hasNext()) {
			Participant participant = (Participant) iterator.next();
			if (participant.getSpendMoney() != 0) {
				return true;
			}
		}
		return false;
	}

	public CyclicBarrier getBarrier() {
		return barrier;
	}

	public void addParticipant(Participant participant) {
		participants.add(participant);
	}

	public void addRoundAuction(RoundAuction round) {
		rounds.add(round);
	}

	public ArrayList<Participant> getAllParticipants() {
		return participants;
	}

	public ArrayList<RoundAuction> getAllRoundsAuction() {
		return rounds;
	}

	public int getRoundsNumber() {
		return rounds.size();
	}

	public boolean isFinished() {
		return isFinished.get();
	}
}
