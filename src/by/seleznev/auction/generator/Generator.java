package by.seleznev.auction.generator;

import java.util.HashSet;
import java.util.Random;

public class Generator {

	private static int idLot;
	private static int idParticipant;

	private Generator() {

	}

	public static int generateIdRound() {
		return ++idLot;
	}

	public static int generateIdParticipant() {
		return ++idParticipant;
	}
	
	public static HashSet<Integer> generateInterestSet(int lotsNumber) {
		int[] array = new int[new Random().nextInt(lotsNumber) + 1];
		for (int i = 0; i < array.length; i++) {
			array[i] = new Random().nextInt(lotsNumber) + 1;
		}
		HashSet<Integer> set = new HashSet<>();
		for (int i = 0; i < array.length; i++) {
			set.add(array[i]);
		}
		return set;
	}
	
	public static int generateParticipantTotalMoney() {
		return new Random().nextInt(50000 - 15000) + 15000;
	}
	
	public static int generateInitLotPrice() {
		return new Random().nextInt(1500 - 1000) + 1000;
	}
}
