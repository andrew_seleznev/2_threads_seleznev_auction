package by.seleznev.auction.report;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;

import by.seleznev.auction.entity.Participant;
import by.seleznev.auction.entity.RoundAuction;

public class ReportAuction {

	static Logger logger = Logger.getLogger(ReportAuction.class);

	public static void printParticipantStatistic(ArrayList<Participant> participants, Participant winner) {
		System.out.println("========== Results after round " + winner.getRoundAuction().getIdRound() + ": Participant "
				+ winner.getIdParticipant() + ", lot: " + winner.getRoundAuction().getIdRound() + ", lot's name: '"
				+ winner.getRoundAuction().getLotName() + "', price: " + winner.getSpendMoney()
				+ " has won! ==========");
		logger.info("Round " + winner.getRoundAuction().getIdRound() + ": Participant " + winner.getIdParticipant()
				+ " has won!");
		for (Iterator<Participant> iterator = participants.iterator(); iterator.hasNext();) {
			Participant participant = iterator.next();
			participant.countMoney(winner.getIdParticipant());
			System.out.println(
					"Participant: " + participant.getIdParticipant() + ", money: " + participant.getTotalMoney());
		}
		logger.info("Round " + winner.getRoundAuction().getIdRound() + " finish");
	}

	public static void printUnsoldLotInformation(ArrayList<Participant> participants) {
		Participant participant = participants.get(0);
		RoundAuction round = participant.getRoundAuction();
		logger.info("Lot " + round.getIdRound() + ", name: " + round.getLotName() + " hasn't been sold");
	}

	public static void printFinalReport(ArrayList<Participant> participants) {
		System.out.println("########## Final report ##########");
		Iterator<Participant> iterator = participants.iterator();
		while (iterator.hasNext()) {
			Participant participant = (Participant) iterator.next();
			System.out.println("Participant: " + participant.getIdParticipant() + " has bought: ");
			HashMap<String, Integer> map = participant.getPurchases();
			for (HashMap.Entry<String, Integer> entry : map.entrySet()) {
				String name = entry.getKey();
				Integer price = entry.getValue();
				System.out.println("--> Lot: '" + name + "', price: " + price);
			}
			System.out.println("--> All money: " + participant.INIT_MONEY);
			System.out.println("--> Remaining money: " + participant.getTotalMoney());
		}
	}
}
