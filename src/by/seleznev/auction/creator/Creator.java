package by.seleznev.auction.creator;

import java.util.ArrayList;
import java.util.MissingResourceException;
import java.util.concurrent.CyclicBarrier;

import org.apache.log4j.Logger;

import by.seleznev.auction.entity.Auction;
import by.seleznev.auction.entity.Participant;
import by.seleznev.auction.entity.RoundAuction;
import by.seleznev.auction.generator.Generator;

public class Creator {
	
	static Logger logger = Logger.getLogger(Creator.class);
	public static final int BASE_STEP_PERCENT = 5;
	public static final int BIDDING_TIME = 5;
	
	public static void registerAllLots(Auction auction) {
		ArrayList<RoundAuction> rounds = auction.getAllRoundsAuction();
		ArrayList<Participant> participants = auction.getAllParticipants();
		CyclicBarrier barrier = auction.getBarrier();
		try {
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"Mona Lisa � Leonardo da Vinci", participants, auction.getBarrier()));
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"The Last Supper � Leonardo da Vinci", participants, barrier));
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"The Creation Of Adam � Michelangelo", participants, barrier));
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"Starry Night � Vincent van Gogh", participants, barrier));
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"The Scream � Edvard Munch", participants, barrier));
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"The Persistence Of Memory � Salvador Dali", participants, barrier));
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"Girl With A Pearl Earring � Johannes Vermeer", participants, barrier));
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"The Night Watch � Rembrandt van Rijn", participants, barrier));
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"Self-Portrait Without Beard � Vincent van Gogh", participants, barrier));
			rounds.add(new RoundAuction(Generator.generateIdRound(), BIDDING_TIME, Generator.generateInitLotPrice(), BASE_STEP_PERCENT,
					"Guernica � Pablo Picasso", participants, barrier));
		} catch (MissingResourceException e) {
			logger.error(e.getMessage());
		}
	}
}
