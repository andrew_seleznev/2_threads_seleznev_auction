package by.seleznev.auction.main;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

import by.seleznev.auction.creator.Creator;
import by.seleznev.auction.entity.Auction;
import by.seleznev.auction.entity.Participant;
import by.seleznev.auction.generator.Generator;

public class AuctionRunner {

	static {
		new DOMConfigurator().doConfigure("conf/log4j.xml", LogManager.getLoggerRepository());
	}

	static Logger logger = Logger.getLogger(AuctionRunner.class);

	public static void main(String[] args) {
		Auction auction = Auction.getInstance();
		Creator.registerAllLots(auction);

		for (int i = 0; i < auction.PARTICIPANTS_NUMBER; i++) {
			Participant participant = new Participant(Generator.generateIdParticipant(),
					Generator.generateParticipantTotalMoney(), auction.getBarrier(), auction.getRoundsNumber(),
					Generator.generateInterestSet(auction.getRoundsNumber()));
			auction.addParticipant(participant);
			participant.start();
		}
		auction.start();
		logger.info("Auction has been started");
	}
}
